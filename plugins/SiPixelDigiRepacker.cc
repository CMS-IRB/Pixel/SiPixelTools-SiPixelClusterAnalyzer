// -*- C++ -*-
//
// Package:    SiPixelTools/SiPixelDigiRepacker
// Class:      SiPixelDigiRepacker
//
/**\class SiPixelDigiRepacker SiPixelDigiRepacker.cc SiPixelTools/SiPixelDigiRepacker/plugins/SiPixelDigiRepacker.cc

 Description: [one line class summary]

 Implementation:
     [Notes on implementation]
*/
//
// Original Author:  Dinko Ferencek
//         Created:  Mon, 25 Apr 2022 10:04:15 GMT
//
//

// system include files
#include <memory>

// user include files
#include "FWCore/Framework/interface/Frameworkfwd.h"
#include "FWCore/Framework/interface/stream/EDProducer.h"

#include "FWCore/Framework/interface/Event.h"
#include "FWCore/Framework/interface/MakerMacros.h"

#include "FWCore/ParameterSet/interface/ParameterSet.h"
#include "FWCore/Utilities/interface/StreamID.h"

#include "DataFormats/Common/interface/DetSetVector.h"
#include "DataFormats/SiPixelDigi/interface/PixelDigi.h"

#include "SiPixelTools/SiPixelClusterAnalyzer/interface/SiPixelDigiPacking.h"

//
// class declaration
//

class SiPixelDigiRepacker : public edm::stream::EDProducer<> {
public:
  explicit SiPixelDigiRepacker(const edm::ParameterSet&);
  ~SiPixelDigiRepacker();

  static void fillDescriptions(edm::ConfigurationDescriptions& descriptions);

private:
  void beginStream(edm::StreamID) override;
  void produce(edm::Event&, const edm::EventSetup&) override;
  void endStream() override;

  //virtual void beginRun(edm::Run const&, edm::EventSetup const&) override;
  //virtual void endRun(edm::Run const&, edm::EventSetup const&) override;
  //virtual void beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;
  //virtual void endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&) override;

  // ----------member data ---------------------------
  const edm::EDGetTokenT<edm::DetSetVector<PixelDigi>> siPixelDigiGetToken_;
  const edm::EDPutTokenT<edm::DetSetVector<PixelDigi>> siPixelDigiPutToken_;

};

//
// constants, enums and typedefs
//


//
// static data member definitions
//

//
// constructors and destructor
//
SiPixelDigiRepacker::SiPixelDigiRepacker(const edm::ParameterSet& iConfig)
 :
  siPixelDigiGetToken_(consumes<edm::DetSetVector<PixelDigi>>(iConfig.getParameter<edm::InputTag>("src"))),
  siPixelDigiPutToken_(produces<edm::DetSetVector<PixelDigi>>())

{

}

SiPixelDigiRepacker::~SiPixelDigiRepacker() {
  // do anything here that needs to be done at destruction time
  // (e.g. close files, deallocate resources etc.)
  //
  // please remove this method altogether if it would be left empty
}

//
// member functions
//

// ------------ method called to produce the data  ------------
void SiPixelDigiRepacker::produce(edm::Event& iEvent, const edm::EventSetup& iSetup) {

  // get input digis
  auto const& inputDigis = iEvent.get(siPixelDigiGetToken_);

  // define output digi collection
  auto outputDigis = std::make_unique<edm::DetSetVector<PixelDigi>>();

  // loop over DetSets
  for (auto const& ds : inputDigis)
  {
    auto rawId = ds.detId();
    edm::DetSet<PixelDigi>* detDigis = nullptr;
    detDigis = &(outputDigis->find_or_insert(rawId));

    // loop over digis in a DetSet
    for (auto const& di : ds)
    {
      // get packed digi data
      auto packedData = di.packedData();

      // unpack the digi information using the right (10_6_X) packing
      auto row  = SiPixelDigiPacking::row(packedData);
      auto col  = SiPixelDigiPacking::column(packedData);
      auto adc  = SiPixelDigiPacking::adc(packedData);
#ifdef CMSSW_12_0_X
      auto flag = SiPixelDigiPacking::flag(packedData);
#endif

      // create a new repacked digi and put it in the output collection
#ifdef CMSSW_12_0_X
      (*detDigis).data.emplace_back(row, col, adc, flag);
#else
      (*detDigis).data.emplace_back(row, col, adc);
#endif
    }
  }

  // put the output collection in the event
  iEvent.put(siPixelDigiPutToken_, std::move(outputDigis));
}

// ------------ method called once each stream before processing any runs, lumis or events  ------------
void SiPixelDigiRepacker::beginStream(edm::StreamID) {
  // please remove this method if not needed
}

// ------------ method called once each stream after processing all runs, lumis and events  ------------
void SiPixelDigiRepacker::endStream() {
  // please remove this method if not needed
}

// ------------ method called when starting to processes a run  ------------
/*
void
SiPixelDigiRepacker::beginRun(edm::Run const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when ending the processing of a run  ------------
/*
void
SiPixelDigiRepacker::endRun(edm::Run const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when starting to processes a luminosity block  ------------
/*
void
SiPixelDigiRepacker::beginLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/

// ------------ method called when ending the processing of a luminosity block  ------------
/*
void
SiPixelDigiRepacker::endLuminosityBlock(edm::LuminosityBlock const&, edm::EventSetup const&)
{
}
*/

// ------------ method fills 'descriptions' with the allowed parameters for the module  ------------
void SiPixelDigiRepacker::fillDescriptions(edm::ConfigurationDescriptions& descriptions) {
  //The following says we do not know what parameters are allowed so do no validation
  // Please change this to state exactly what you do use, even if it is no parameters
  edm::ParameterSetDescription desc;
  desc.setUnknown();
  descriptions.addDefault(desc);
}

//define this as a plug-in
DEFINE_FWK_MODULE(SiPixelDigiRepacker);
