#ifndef SiPixelTools_SiPixelClusterAnalyzer_SiPixelDigiPacking_h
#define SiPixelTools_SiPixelClusterAnalyzer_SiPixelDigiPacking_h

#include "DataFormats/SiPixelDigi/interface/PixelDigi.h"

class SiPixelDigiPacking {

  public:
    // convert channel number into pixel coordinates
    static std::pair<int, int> channelToPixel(int ch) {

      int row = (ch >> column_width) & row_mask;
      int col = ch & column_mask;

      return std::pair<int, int>(row, col);
    }

    // access to digi information
    static int row(const PixelDigi::PackedDigiType& theData) {
      return (theData >> row_shift) & row_mask;
    }
    static int column(const PixelDigi::PackedDigiType& theData) {
      return (theData >> column_shift) & column_mask;
    }
    static int flag(const PixelDigi::PackedDigiType& theData) {
      return (theData >> flag_shift) & flag_mask;
    }
    static unsigned short adc(const PixelDigi::PackedDigiType& theData) {
      return (theData >> adc_shift) & adc_mask;
    }

  private:
    constexpr static unsigned int row_width = 11;
    constexpr static unsigned int column_width = 11;
    constexpr static unsigned int flag_width = 0;
    constexpr static unsigned int adc_width = 10;

    constexpr static unsigned int row_mask = (~(~0U << row_width));
    constexpr static unsigned int column_mask = (~(~0U << column_width));
    constexpr static unsigned int flag_mask = (~(~0U << flag_width));
    constexpr static unsigned int adc_mask = (~(~0U << adc_width));

    constexpr static unsigned int row_shift = 0;
    constexpr static unsigned int column_shift = (row_shift + row_width);
    constexpr static unsigned int flag_shift = (column_shift + column_width);
    constexpr static unsigned int adc_shift = (flag_shift + flag_width);
};

#endif
