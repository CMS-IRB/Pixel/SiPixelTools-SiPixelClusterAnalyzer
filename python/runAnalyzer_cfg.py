import FWCore.ParameterSet.Config as cms

from FWCore.ParameterSet.VarParsing import VarParsing

###############################
####### Parameters ############
###############################

options = VarParsing ('python')

options.register('reportEvery', 10,
    VarParsing.multiplicity.singleton,
    VarParsing.varType.int,
    "Report every N events (default is N=10)"
)
options.register('wantSummary', False,
    VarParsing.multiplicity.singleton,
    VarParsing.varType.bool,
    "Print out trigger and timing summary"
)
options.register('verbose', False,
    VarParsing.multiplicity.singleton,
    VarParsing.varType.bool,
    "Verbose printout"
)
options.register('postSplitting', False,
    VarParsing.multiplicity.singleton,
    VarParsing.varType.bool,
    "Apply cluster splitting"
)
options.register('addPixels', False,
    VarParsing.multiplicity.singleton,
    VarParsing.varType.bool,
    "Add cluster pixels to the output tree"
)
options.register('morphing', False,
    VarParsing.multiplicity.singleton,
    VarParsing.varType.bool,
    "Apply digi morphing"
)


## changing default values for arguments already registered by the Framework
options.setDefault('maxEvents', 100)
options.setDefault('outputFile', 'cluster_tree.root')

options.parseArguments()

from Configuration.Eras.Era_Run2_2018_cff import Run2_2018

process = cms.Process("USER",Run2_2018)

process.load("FWCore.MessageService.MessageLogger_cfi")
process.MessageLogger.cerr.FwkReport.reportEvery = options.reportEvery

## For loading geometry and calibrations
process.load('RecoLocalTracker.SiPixelRecHits.PixelCPEGeneric_cfi')
process.load('Configuration.StandardSequences.GeometryRecoDB_cff')
process.load('Configuration.StandardSequences.MagneticField_cff')
process.load('Configuration.StandardSequences.FrontierConditions_GlobalTag_cff')
from Configuration.AlCa.GlobalTag import GlobalTag
process.GlobalTag = GlobalTag(process.GlobalTag, 'auto:phase1_2018_realistic', '')

## Events to process
process.maxEvents = cms.untracked.PSet( input = cms.untracked.int32(options.maxEvents) )

## Input files
process.source = cms.Source("PoolSource",
    fileNames = cms.untracked.vstring(
        # /QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/RunIISummer19UL18RECO-PREMIX_RECODEBUG_PREMIX_RECODEBUG_PREMIX_RECODEBUG_106X_upgrade2018_realistic_v11_L1v1-v3/GEN-SIM-RECODEBUG
        #'root://xrootd-cms.infn.it///store/mc/RunIISummer19UL18RECO/QCD_Pt_800to1000_TuneCP5_13TeV_pythia8/GEN-SIM-RECODEBUG/PREMIX_RECODEBUG_PREMIX_RECODEBUG_PREMIX_RECODEBUG_106X_upgrade2018_realistic_v11_L1v1-v3/230000/0EE8D5C4-9466-9D4A-9FBF-2789418554CA.root'
        # /RelValZpTT_1500_13UP18/CMSSW_10_6_2-PUpmx25ns_106X_upgrade2018_realistic_v6_ul18hlt_premix_rs-v1/FEVTDEBUGHLT
        #'root://xrootd-cms.infn.it///store/relval/CMSSW_10_6_2/RelValZpTT_1500_13UP18/FEVTDEBUGHLT/PUpmx25ns_106X_upgrade2018_realistic_v6_ul18hlt_premix_rs-v1/20000/002A53C1-E7DE-4D4F-AA8F-5434259D4652.root'
        # /RelValZpTT_1500_13UP18/CMSSW_10_6_2-PU25ns_106X_upgrade2018_realistic_v6-v2/FEVTDEBUGHLT
        #'root://xrootd-cms.infn.it///store/relval/CMSSW_10_6_2/RelValZpTT_1500_13UP18/FEVTDEBUGHLT/PU25ns_106X_upgrade2018_realistic_v6-v2/10000/0243CC8A-7C8A-964A-B8CE-6CC68FAF653D.root'
        'file:/STORE/ferencek/Pixel/mc/relval/CMSSW_10_6_2/RelValZpTT_1500_13UP18/FEVTDEBUGHLT/PU25ns_106X_upgrade2018_realistic_v6-v2/10000/0243CC8A-7C8A-964A-B8CE-6CC68FAF653D.root'
    )
)

if len(options.inputFiles):
    process.source.fileNames = cms.untracked.vstring(options.inputFiles)

## Options and Output Report
process.options   = cms.untracked.PSet(
    wantSummary = cms.untracked.bool(options.wantSummary),
    allowUnscheduled = cms.untracked.bool(False)
)

process.TFileService = cms.Service("TFileService",
    fileName = cms.string(options.outputFile)
)

## Select generated taus
process.load("SimGeneral.HepPDTESSource.pythiapdt_cfi")
process.generatedTaus = cms.EDProducer("GenParticlePruner",
    src = cms.InputTag("genParticles"),
    select = cms.vstring(
        "drop  *"
        ,"keep (abs(pdgId) = 15) & status = 2" # keeps taus
    )
)

## Load SiPixelClusterizer
from RecoLocalTracker.SiPixelClusterizer.SiPixelClusterizerPreSplitting_cfi import _siPixelClusters
if options.morphing:
    process.simSiPixelDigisRepacked = cms.EDProducer("SiPixelDigiRepacker",
        src = cms.InputTag('simSiPixelDigis')
    )
    process.load("RecoLocalTracker.SiPixelDigiReProducers.siPixelDigisMorphed_cfi")
    process.siPixelDigisMorphed.src = 'simSiPixelDigisRepacked'
    #process.siPixelDigisMorphed.fakeAdc = 50
    process.siPixelClustersPreSplitting = _siPixelClusters.clone(
        src = 'siPixelDigisMorphed'
    )
else:
    process.simSiPixelDigisRepacked = cms.EDProducer("SiPixelDigiRepacker",
        src = cms.InputTag('simSiPixelDigis')
    )
    process.siPixelClustersPreSplitting = _siPixelClusters.clone(
        #src = 'siPixelClusters::RECO',
        #ClusterMode = 'PixelThresholdReclusterizer'
        src = 'simSiPixelDigisRepacked'
    )

## Load SiPixelClusterAnalyzer
process.siPixelClusterAnalyzer = cms.EDAnalyzer("SiPixelClusterAnalyzer",
    clusters = cms.InputTag("siPixelClusters::RECO" if options.postSplitting else "siPixelClustersPreSplitting"),
    digiSimLinks = cms.InputTag("simSiPixelDigis"),
    jets = cms.InputTag("ak4PFJets"),
    taus = cms.InputTag("generatedTaus"),
    vertices = cms.InputTag("offlinePrimaryVertices"),
    pixelCPE = cms.string('PixelCPEGeneric'),
    ptMin = cms.double(20.),
    dRMin = cms.double(0.4),
    addPixels = cms.bool(options.addPixels),
    verbose = cms.untracked.bool(options.verbose)
)

## Output file
#process.out = cms.OutputModule("PoolOutputModule",
    #fileName = cms.untracked.string('RECODEBUG_mergedPixelClusters.root'),
    #outputCommands = cms.untracked.vstring(
        #"keep *"
    #)
#)

## Let it run
if options.postSplitting:
    process.p = cms.Path(process.generatedTaus*process.siPixelClusterAnalyzer)
elif options.morphing:
    process.p = cms.Path((process.generatedTaus+(process.simSiPixelDigisRepacked*process.siPixelDigisMorphed*process.siPixelClustersPreSplitting))*process.siPixelClusterAnalyzer)
else:
    process.p = cms.Path((process.generatedTaus+process.simSiPixelDigisRepacked*process.siPixelClustersPreSplitting)*process.siPixelClusterAnalyzer)

#process.outpath = cms.EndPath(process.out)
