# SiPixelClusterAnalyzer

Pixel cluster analyzer package.

## Software setup

Prepare your working directory with CMSSW
```
source /cvmfs/cms.cern.ch/cmsset_default.sh
```
### CMSSW_10_6_X
```
export SCRAM_ARCH=slc7_amd64_gcc700
cmsrel CMSSW_10_6_25
cd CMSSW_10_6_25/src/
```
### CMSSW_12_0_X
```
export SCRAM_ARCH=slc7_amd64_gcc900
cmsrel CMSSW_12_0_3_patch2
cd CMSSW_12_0_3_patch2/src/
git cms-merge-topic ferencek:DropFakePixelsFromFinalClusters_from-CMSSW_12_0_3_patch2
```
```
cmsenv
git clone https://gitlab.cern.ch/CMS-IRB/Pixel/SiPixelTools-SiPixelClusterAnalyzer.git SiPixelTools/SiPixelClusterAnalyzer
```
### CMSSW_10_6_X
```
scram b -j 4
```
### CMSSW_12_0_X

CMSSW_12_0_X is required to be able to run the pixel digi morphing algorithm.
```
scram b -j 4 USER_CXXFLAGS="-DCMSSW_12_0_X"
```
```
cd SiPixelTools/SiPixelClusterAnalyzer/python/
```
Note the user flag above which is needed to take into account changes in the CMSSW framework between CMSSW_10_6_X and CMSSW_12_0_X. Since there were also changes in the way the digi information is packed between CMSSW_10_6_X and CMSSW_12_0_X, custom unpacking functions have to be used to correctly unpack the digi information in CMSSW_12_0_X when using CMSSW_10_6_X samples as input. For this purpose a dedicated pixel digi repacker was implemented. Consequently, the examples below with the digi morhing enabled should be run only in CMSSW_12_0_X.

## Check event content

<!--Initialize GRID proxy
```
voms-proxy-init -rfc -voms cms
```-->
To check the event content of the input file, run `edmDumpEventContent` on the input file as in the following example for a file from
/RelValZpTT_1500_13UP18/CMSSW_10_6_2-PU25ns_106X_upgrade2018_realistic_v6-v2/FEVTDEBUGHLT
<!--```
edmDumpEventContent 'root://xrootd-cms.infn.it///store/relval/CMSSW_10_6_2/RelValZpTT_1500_13UP18/FEVTDEBUGHLT/PU25ns_106X_upgrade2018_realistic_v6-v2/10000/0243CC8A-7C8A-964A-B8CE-6CC68FAF653D.root'
```-->
```
edmDumpEventContent /STORE/ferencek/Pixel/mc/relval/CMSSW_10_6_2/RelValZpTT_1500_13UP18/FEVTDEBUGHLT/PU25ns_106X_upgrade2018_realistic_v6-v2/10000/0243CC8A-7C8A-964A-B8CE-6CC68FAF653D.root
```
The above data set was produced in CMSSW_10_6_2.

## Inspect generated particles

To inspect generated particles, run
```
cmsRun inspectEvents_cfg.py maxEvents=1 skipEvents=0 inputFiles=file:/STORE/ferencek/Pixel/mc/relval/CMSSW_10_6_2/RelValZpTT_1500_13UP18/FEVTDEBUGHLT/PU25ns_106X_upgrade2018_realistic_v6-v2/10000/0243CC8A-7C8A-964A-B8CE-6CC68FAF653D.root
```

## Produce ntuples

Run the CMSSW analyzer to produce the ntuples
```
cmsRun runAnalyzer_cfg.py maxEvents=100 reportEvery=1 wantSummary=True
```
To process the full data set, run the following command which will submit jobs to Condor
```
python submitNtupleJobs.py -c condor_ntuple_jobs -s runAnalyzer_cfg.py -i /STORE/ferencek/Pixel/mc/relval/CMSSW_10_6_2/RelValZpTT_1500_13UP18/FEVTDEBUGHLT/PU25ns_106X_upgrade2018_realistic_v6-v2/10000/ -o /STORE/ferencek/Pixel/mc/PixelClusterTrees/MergedClusters/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/
```
Check the status of the Condor jobs by running
```
condor_q
```
To check if there are any failed jobs, enter the most recent condor jobs folder
```
cd $(ls -td condor_ntuple_jobs_*/ | head -n 1)
```
and execute
```
grep 'return value' *.log | grep -v 'return value 0'
```
In case there are some failed jobs, you can resubmit them using the following one-liners
```
grep 'return value' *.log | grep -v 'return value 0' | awk '{print $1}' | sed 's/://' > failed_jobs.txt

for f in $(cat failed_jobs.txt); do rm $f; done

for f in $(cat failed_jobs.txt | sed 's/\.log/\.jdl/'); do condor_submit $f; done
```
Here is an example of ntuple production with cluster splitting
```
python submitNtupleJobs.py -c condor_ntuple_jobs_postSplitting -s runAnalyzer_cfg.py --postSplitting -i /STORE/ferencek/Pixel/mc/relval/CMSSW_10_6_2/RelValZpTT_1500_13UP18/FEVTDEBUGHLT/PU25ns_106X_upgrade2018_realistic_v6-v2/10000/ -o /STORE/ferencek/Pixel/mc/PixelClusterTrees/MergedClustersPostSplitting/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/
```
or digi morphing applied
```
python submitNtupleJobs.py -c condor_ntuple_jobs_morphing -s runAnalyzer_cfg.py --morphing -i /STORE/ferencek/Pixel/mc/relval/CMSSW_10_6_2/RelValZpTT_1500_13UP18/FEVTDEBUGHLT/PU25ns_106X_upgrade2018_realistic_v6-v2/10000/ -o /STORE/ferencek/Pixel/mc/PixelClusterTrees/MorphedClusters/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/
```
or with cluster pixels stored
```
python submitNtupleJobs.py -c condor_ntuple_jobs_withPixels -s runAnalyzer_cfg.py --addPixels -i /STORE/ferencek/Pixel/mc/relval/CMSSW_10_6_2/RelValZpTT_1500_13UP18/FEVTDEBUGHLT/PU25ns_106X_upgrade2018_realistic_v6-v2/10000/ -o /STORE/ferencek/Pixel/mc/PixelClusterTrees/MergedClustersWithPixels/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/
```
```
python submitNtupleJobs.py -c condor_ntuple_jobs_morphing_withPixels -s runAnalyzer_cfg.py --morphing --addPixels -i /STORE/ferencek/Pixel/mc/relval/CMSSW_10_6_2/RelValZpTT_1500_13UP18/FEVTDEBUGHLT/PU25ns_106X_upgrade2018_realistic_v6-v2/10000/ -o /STORE/ferencek/Pixel/mc/PixelClusterTrees/MorphedClustersWithPixels/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/
```


## Run analysis

Run the following Python script to analyze pixel clusters
```
python analyzeClusters.py
```
Check the available command-line options by running
```
python analyzeClusters.py --help
```
To process the full data set, run the following command which will submit jobs to Condor
```
python submitAnalysisJobs.py -c condor_analysis_jobs -s analyzeClusters.py -i /STORE/ferencek/Pixel/mc/PixelClusterTrees/MergedClusters/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/
```
Check the status of the Condor jobs by running
```
condor_q
```
To check if there are any failed jobs, enter the most recent condor jobs folder
```
cd $(ls -td condor_analysis_jobs_*/ | head -n 1)
```
and execute
```
grep 'return value' *.log | grep -v 'return value 0'
```
In case there are some failed jobs, you can resubmit them using the following one-liners
```
grep 'return value' *.log | grep -v 'return value 0' | awk '{print $1}' | sed 's/://' > failed_jobs.txt

for f in $(cat failed_jobs.txt); do rm $f; done

for f in $(cat failed_jobs.txt | sed 's/\.log/\.jdl/'); do condor_submit $f; done
```
Finally, merge all the output histogram files into one file
```
hadd cluster_histograms.root cluster_histograms_*.root
```
