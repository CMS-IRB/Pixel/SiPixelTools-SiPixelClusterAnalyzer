import ROOT
import glob
import sys
# define commandline arguments
from optparse import OptionParser
from collections import Counter

usage = "Usage: python %prog [options] \n\nExample: python %prog [--maxEvents=100 --reportEvery=1 --inputFile=cluster_tree.root]"

parser = OptionParser(usage=usage)

parser.add_option('--maxEvents', type='int', action='store',
                  default=10,
                  dest='maxEvents',
                  help='Number of events to process. -1 means all events')

parser.add_option('--startEvent', type='int', action='store',
                  default=0,
                  dest='startEvent',
                  help='Starting event index from the root file')

parser.add_option('--reportEvery', type='int', action='store',
                  default=1,
                  dest='reportEvery',
                  help='Report every N events (default value is 100)')

parser.add_option('--inputFile', type='string', action='store',
                  default='cluster_tree.root',
                  dest='inputFile',
                  help='Input file (default value is \'cluster_tree.root\')')

parser.add_option('--trackString', type='string', action='store',
                  default='',
                  dest='trackString',
                  help='Input string for track information in following format: iterator_trk_Id_bx_Id_evt_Id_det_Id')

(options, args) = parser.parse_args()


inputFile = options.inputFile
maxEvents = options.maxEvents
trkString = options.trackString
det_id = trkString.split("_")[-1]
print det_id
if trkString !='':
    mode = 'plotCluster'
else:
    mode = 'printCluster'


t = ROOT.TChain()
if ".root" not in inputFile:
    for file in glob.glob(inputFile+"/*"):
        #print file
        if file.endswith(".root"):
            t.Add(file+"/siPixelClusterAnalyzer/t_clusters")
else:
    # get pointer to the ROOT tree
    t.Add(inputFile+"/siPixelClusterAnalyzer/t_clusters")

# create output file
#fOut = ROOT.TFile(options.outputFile, "RECREATE")

# total number of events
nEntries = t.GetEntries()
clus_plot = ROOT.TH2F('h',";pix Y position;pix X position",450,0,450,200,0,200)
#print nEntries
# loop over events
trk_dict = {}
for e in range(options.startEvent, nEntries):
    if maxEvents>0 and (e+1)>maxEvents :
        break

    # get event
    t.GetEntry(e)

    nClusters_BPixL1=0
    # loop over clusters
    for c in range(0, t.track_Id.size()):

        clus_Id = t.track_clusId[c]
        #if t.clus_subdet[clus_Id]==0 and t.clus_lyrdsk[clus_Id]==1 and abs(t.clus_glZ[clus_Id])>20.0:
        if t.clus_subdet[clus_Id]==0 and t.clus_lyrdsk[clus_Id]==1:

            trk_Id = t.track_Id[c]
            bx_Id = t.track_bx[c]
            evt_Id = t.track_evtId[c]
            det_Id = t.clus_detId[clus_Id]
            trk_str = str(e)+"_"+str(trk_Id)+"_"+str(bx_Id)+"_"+str(evt_Id)+"_"+str(det_Id)
            #trk_str = str(e)+"_"+str(trk_Id)+"_"+str(det_Id)
            if trk_str not in trk_dict:
               trk_dict[trk_str]= [clus_Id]
            else:
               trk_dict[trk_str].append(clus_Id)
            clus = clus_Id
	    for c in range(0, t.clus_pixADC[clus].size()):
	        clus_X = t.clus_pixX[clus][c]
	        clus_Y = t.clus_pixY[clus][c]
	        clus_ADC = t.clus_pixADC[clus][c]
	        #if clus_ADC == 1000:
	    	#    print str(e)+"_"+str(trk_Id)+"_"+str(bx_Id)+"_"+str(evt_Id)+"_"+str(det_Id)+":  "+str(clus)+": X: "+str(clus_X)+", Y: "+str(clus_Y)+", ADC: "+str(clus_ADC)
            
def plotCluster(clus_list):
    list_X =[]
    list_Y =[]
    pix = {}
    pix_c = 0
    for clus in clus_list: 
        for c in range(0, t.clus_pixADC[clus].size()):
            clus_X = t.clus_pixX[clus][c]
            clus_Y = t.clus_pixY[clus][c]
            clus_ADC = t.clus_pixADC[clus][c]
            list_X.append(clus_X)
            list_Y.append(clus_Y)
            canvas.cd()
            #pix[pix_c] = ROOT.TBox(clus_Y-0.5, clus_X-0.5, clus_Y+0.5, clus_X+0.5)
            #pix[pix_c] = ROOT.TBox(0.3, 0.3, 0.5, 0.5)
            clus_plot.SetBinContent(clus_Y,clus_X,clus_ADC)
            if clus_ADC == 1000:
                print c
            #clus_plot.SetBinContent(clus_Y,clus_X,1.0)
            print str(clus)+": X: "+str(clus_X)+", Y: "+str(clus_Y)+", ADC: "+str(clus_ADC)
            #pix[pix_c].SetFillStyle(3005)
            #pix[pix_c].SetFillColor(ROOT.kBlue-4)
            #pix[pix_c].Draw()
            #pix_c+=1
    print 'Y:'+str(list_Y)
    print 'X:'+str(list_X)
    clus_plot.GetXaxis().SetRangeUser(min(list_Y)-10,max(list_Y)+10)
    clus_plot.GetYaxis().SetRangeUser(min(list_X)-10,max(list_X)+10)
    canvas.Update()

def markCluster(clus_list):
    pix_m = {}
    #pix_m = []
    pix_c = 0
    for clus in clus_list: 
        for c in range(0, t.clus_pixADC[clus].size()):
            clus_X = t.clus_pixX[clus][c]
            clus_Y = t.clus_pixY[clus][c]
            clus_ADC = t.clus_pixADC[clus][c]
            canvas.cd()
            #pix_m[pix_c] = ROOT.TBox(clus_Y-0.5, clus_X-0.5, clus_Y+0.5, clus_X+0.5)
            #pix_m[pix_c] = ROOT.TBox(0.5, 0.5, 0.7, 0.7)
            #pix_m[pix_c].SetFillStyle(3001)
            #pix_m[pix_c].SetFillColor(ROOT.kRed)
            #pix_m[pix_c].Draw()
            #pix_c+=1
            

 
if mode=='printCluster':
    for trk in trk_dict:
	clusters  =  trk_dict[trk]
        #print trk+":"+str(clusters)
        for clus in clusters: 
		for c in range(0, t.clus_pixADC[clus].size()):
		    clus_X = t.clus_pixX[clus][c]
		    clus_Y = t.clus_pixY[clus][c]
		    clus_ADC = t.clus_pixADC[clus][c]
		    #if clus_ADC == 1000:
			#print str(clus)+": X: "+str(clus_X)+", Y: "+str(clus_Y)+", ADC: "+str(clus_ADC)
            
elif mode=='plotCluster':
   canvas = ROOT.TCanvas('c','c',800,800)
   clus_plot.SetName("Barrel L1 Track "+trk_str)
   ROOT.gStyle.SetOptStat(0)
   clus_plot.Draw("colz")
   #for trk in trk_dict:
   #    if "_"+det_id in trk and trk!=trkString:
   #        plotCluster(trk_dict[trk]) 
           
   if trkString in trk_dict:
       plotCluster(trk_dict[trkString])
   else:
       print "\t*ERROR*: Track String not found in track dictionary. Run more events or check your string."
    
   if 'Merged' in inputFile:
       ROOT.gPad.SaveAs(trkString+"_nominal.png")
   else:
       ROOT.gPad.SaveAs(trkString+"_morph.png")

