# define commandline arguments
from optparse import OptionParser

usage = "Usage: python %prog [options] \n\nExample: python %prog [--maxEvents=100 --reportEvery=1 --inputFile=cluster_tree.root]"

parser = OptionParser(usage=usage)

parser.add_option('--maxEvents', type='int', action='store',
                  default=10,
                  dest='maxEvents',
                  help='Number of events to process. -1 means all events')

parser.add_option('--reportEvery', type='int', action='store',
                  default=1,
                  dest='reportEvery',
                  help='Report every N events (default value is 100)')

parser.add_option('--inputFile', type='string', action='store',
                  default='cluster_tree.root',
                  dest='inputFile',
                  help='Input file (default value is \'cluster_tree.root\')')

parser.add_option('--outputFile', type='string', action='store',
                  default='cluster_histograms.root',
                  dest='outputFile',
                  help='Output file (default value is \'cluster_histograms.root\')')

parser.add_option("--clusters", dest="clusters", action='store_true',
                  help="Print out clusters",
                  default=False)

parser.add_option("--pixels", dest="pixels", action='store_true',
                  help="Print out pixels",
                  default=False)

parser.add_option("--jets", dest="jets", action='store_true',
                  help="Print out jets",
                  default=False)

parser.add_option("--vtx", dest="vtx", action='store_true',
                  help="Print out the primary vertex",
                  default=False)

(options, args) = parser.parse_args()


                 

import ROOT
import glob

# configure ROOT's behavior
ROOT.gROOT.SetBatch()
ROOT.gStyle.SetOptStat("nemruoi")
ROOT.gROOT.ForceStyle()


# open input file
t = ROOT.TChain()
if ".root" not in options.inputFile:
    for file in glob.glob(options.inputFile+"/*"):
        #print file
        if file.endswith(".root"):
            t.Add(file+"/siPixelClusterAnalyzer/t_clusters")
else:
    # get pointer to the ROOT tree
    t.Add(options.inputFile+"/siPixelClusterAnalyzer/t_clusters")

# create output file
fOut = ROOT.TFile(options.outputFile, "RECREATE")

# define histograms
h_nClusters = ROOT.TH1F("h_nClusters", ";Pixel cluster multiplicity;", 20000, 0., 20000.)
h_nClustersSel = ROOT.TH1F("h_nClustersSel", ";Pixel cluster multiplicity;", 20000, 0., 20000.)
h_nJets = ROOT.TH1F("h_nJets", "Jet p_{T}>20.0 GeV;Jet multiplicity;", 50, 0., 50.)
h_dR= ROOT.TH1F("h_dR","deltaR:",200,0.,20.)


h_dR_min_BPix= ROOT.TH1F("h_dR_min_BPix","deltaR:",200,0.,20.)
h_dR_min_FPix= ROOT.TH1F("h_dR_min_FPix","deltaR:",200,0.,20.)

h_dR_min_BPixL1=ROOT.TH1F("h_dR_min_BPixL1","BPixL1:",200,0.,20.)
h_dR_min_BPixL2=ROOT.TH1F("h_dR_min_BPixL2","BPixL2:",200,0.,20.)
h_dR_min_BPixL3=ROOT.TH1F("h_dR_min_BPixL3","BPixL3:",200,0.,20.)
h_dR_min_BPixL4=ROOT.TH1F("h_dR_min_BPixL4","BPixL4:",200,0.,20.)

h_dR_min_FPixD1=ROOT.TH1F("h_dR_min_FPixD1","FPixD1:",200,0.,20.)
h_dR_min_FPixD2=ROOT.TH1F("h_dR_min_FPixD2","FPixD2:",200,0.,20.)
h_dR_min_FPixD3=ROOT.TH1F("h_dR_min_FPixD3","FPixD3:",200,0.,20.)

nClusterbin=400
nClustermin=0.
nClustermax=2400.

h_nClustersSel_BPix=ROOT.TH1F("h_nClustersSel_BPix", ";Pixel cluster multiplicity;", nClusterbin, nClustermin, nClustermax)
h_nClustersSel_BPixL1=ROOT.TH1F("h_nClustersSel_BPixL1", ";Pixel cluster multiplicity;", nClusterbin, nClustermin, nClustermax)
h_nClustersSel_BPixL2=ROOT.TH1F("h_nClustersSel_BPixL2", ";Pixel cluster multiplicity;", nClusterbin, nClustermin, nClustermax)
h_nClustersSel_BPixL3=ROOT.TH1F("h_nClustersSel_BPixL3", ";Pixel cluster multiplicity;", nClusterbin, nClustermin, nClustermax)
h_nClustersSel_BPixL4=ROOT.TH1F("h_nClustersSel_BPixL4", ";Pixel cluster multiplicity;", nClusterbin, nClustermin, nClustermax)
    
nClusterSbin=400
nClusterSmin=0.
nClusterSmax=8000.  
    
h_nClusters_BPix=ROOT.TH1F("h_nClusters_BPix", ";Pixel cluster multiplicity;", nClusterSbin, nClusterSmin, nClusterSmax)
h_nClusters_BPixL1=ROOT.TH1F("h_nClusters_BPixL1", ";Pixel cluster multiplicity;", nClusterSbin, nClusterSmin, nClusterSmax)
h_nClusters_BPixL2=ROOT.TH1F("h_nClusters_BPixL2", ";Pixel cluster multiplicity;", nClusterSbin, nClusterSmin, nClusterSmax)
h_nClusters_BPixL3=ROOT.TH1F("h_nClusters_BPixL3", ";Pixel cluster multiplicity;", nClusterSbin, nClusterSmin, nClusterSmax)
h_nClusters_BPixL4=ROOT.TH1F("h_nClusters_BPixL4", ";Pixel cluster multiplicity;", nClusterSbin, nClusterSmin, nClusterSmax)
 
nClusterFSbin=400
nClusterFSmin=0.
nClusterFSmax=1200. 
    
h_nClustersSel_FPix=ROOT.TH1F("h_nClustersSel_FPix", ";Pixel cluster multiplicity;", nClusterFSbin, nClusterFSmin, nClusterFSmax)
h_nClustersSel_FPixD1=ROOT.TH1F("h_nClustersSel_FPixD1", ";Pixel cluster multiplicity;", nClusterFSbin, nClusterFSmin, nClusterFSmax)
h_nClustersSel_FPixD2=ROOT.TH1F("h_nClustersSel_FPixD2", ";Pixel cluster multiplicity;", nClusterFSbin, nClusterFSmin, nClusterFSmax)
h_nClustersSel_FPixD3=ROOT.TH1F("h_nClustersSel_FPixD3", ";Pixel cluster multiplicity;", nClusterFSbin, nClusterFSmin, nClusterFSmax)
    
nClusterFbin=400
nClusterFmin=0.
nClusterFmax=6000. 
    
h_nClusters_FPix=ROOT.TH1F("h_nClusters_FPix", ";Pixel cluster multiplicity;", nClusterFbin, nClusterFmin, nClusterFmax)
h_nClusters_FPixD1=ROOT.TH1F("h_nClusters_FPixD1", ";Pixel cluster multiplicity;", nClusterFbin, nClusterFmin, nClusterFmax)
h_nClusters_FPixD2=ROOT.TH1F("h_nClusters_FPixD2", ";Pixel cluster multiplicity;", nClusterFbin, nClusterFmin, nClusterFmax)
h_nClusters_FPixD3=ROOT.TH1F("h_nClusters_FPixD3", ";Pixel cluster multiplicity;", nClusterFbin, nClusterFmin, nClusterFmax)


#2D
jetptbins=800
jetptmin=0
jetptmax=800

dRbins=40
dRmin=0
dRmax=0.4

Zbins=200
Zmin=-40
Zmax=40

Phibins=340
Phimin=-3.2
Phimax=3.2

h2_Cluster_Z_vs_phi_BPixL1_ratio=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL1_ratio",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_BPixL2_ratio=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL2_ratio",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_BPixL3_ratio=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL3_ratio",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_BPixL4_ratio=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL4_ratio",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)

h2_Cluster_Z_vs_phi_FPixD1_ratio=ROOT.TH2F("h2_Cluster_Z_vs_phi_FPixD1_ratio",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_FPixD2_ratio=ROOT.TH2F("h2_Cluster_Z_vs_phi_FPixD2_ratio",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_FPixD3_ratio=ROOT.TH2F("h2_Cluster_Z_vs_phi_FPixD3_ratio",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)

h2_Cluster_Z_vs_phi_BPixL1_merged=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL1_merged",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_BPixL2_merged=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL2_merged",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_BPixL3_merged=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL3_merged",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_BPixL4_merged=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL4_merged",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)

h2_Cluster_Z_vs_phi_FPixD1_merged=ROOT.TH2F("h2_Cluster_Z_vs_phi_FPixD1_merged",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_FPixD2_merged=ROOT.TH2F("h2_Cluster_Z_vs_phi_FPixD2_merged",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_FPixD3_merged=ROOT.TH2F("h2_Cluster_Z_vs_phi_FPixD3_merged",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)

h1_Pixel_ADC_BPixL1=ROOT.TH1F("h1_Pixel_ADC_BPixL1",";Pixel ADC; Pixel Multiplicity", 500, 0, 50000)
h1_Cluster_Z_vs_nPixels_BPixL1=ROOT.TH1F("h1_Cluster_Z_vs_nPixels_BPixL1",";global Z (cm); Pixel Multiplicity", Zbins, Zmin, Zmax)
h2_Cluster_Z_vs_ntracks_BPixL1=ROOT.TH2F("h2_Cluster_Z_vs_ntracks_BPixL1",";global Z (cm); Total no. of tracks", Zbins, Zmin, Zmax, 49,1,50)
h2_Cluster_Z_vs_phi_BPixL1=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL1",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_BPixL2=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL2",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_BPixL3=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL3",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_BPixL4=ROOT.TH2F("h2_Cluster_Z_vs_phi_BPixL4",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)

h2_Cluster_Z_vs_phi_FPixD1=ROOT.TH2F("h2_Cluster_Z_vs_phi_FPixD1",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_FPixD2=ROOT.TH2F("h2_Cluster_Z_vs_phi_FPixD2",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)
h2_Cluster_Z_vs_phi_FPixD3=ROOT.TH2F("h2_Cluster_Z_vs_phi_FPixD3",";global Z (cm); global #Phi", Zbins, Zmin, Zmax, Phibins,Phimin,Phimax)

h2_Cluster_dR_vs_jetpt_BPixL1=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_BPixL1",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)
h2_Cluster_dR_vs_jetpt_BPixL2=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_BPixL2",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)
h2_Cluster_dR_vs_jetpt_BPixL3=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_BPixL3",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)
h2_Cluster_dR_vs_jetpt_BPixL4=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_BPixL4",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)

h2_Cluster_dR_vs_jetpt_FPixD1=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_FPixD1",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)
h2_Cluster_dR_vs_jetpt_FPixD2=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_FPixD2",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)
h2_Cluster_dR_vs_jetpt_FPixD3=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_FPixD3",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)


h2_Cluster_dR_vs_jetpt_BPixL1_merged=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_BPixL1_merged",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)
h2_Cluster_dR_vs_jetpt_BPixL2_merged=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_BPixL2_merged",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)
h2_Cluster_dR_vs_jetpt_BPixL3_merged=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_BPixL3_merged",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)
h2_Cluster_dR_vs_jetpt_BPixL4_merged=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_BPixL4_merged",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)

h2_Cluster_dR_vs_jetpt_FPixD1_merged=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_FPixD1_merged",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)
h2_Cluster_dR_vs_jetpt_FPixD2_merged=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_FPixD2_merged",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)
h2_Cluster_dR_vs_jetpt_FPixD3_merged=ROOT.TH2F("h2_Cluster_dR_vs_jetpt_FPixD3_merged",";p_{T} (GeV); #DeltaR", jetptbins, jetptmin, jetptmax, dRbins,dRmin,dRmax)


# total number of events
nEntries = t.GetEntries()
#print nEntries
# loop over events
for e in range(0, nEntries):
    if options.maxEvents > 0 and (e+1) > options.maxEvents :
        break
    if e % options.reportEvery == 0 :
        print ('Event: %i' % (e+1) )

    # get event
    t.GetEntry(e)

    # fill histograms
    h_nClusters.Fill(t.clus_size.size())
    h_nJets.Fill(t.jet_px.size())
    
    nClustersSel=0
    
    nClustersSel_BPix=0
    nClustersSel_BPixL1=0
    nClustersSel_BPixL2=0
    nClustersSel_BPixL3=0
    nClustersSel_BPixL4=0
    
    nClusters_BPix=0
    nClusters_BPixL1=0
    nClusters_BPixL2=0
    nClusters_BPixL3=0
    nClusters_BPixL4=0
    
    nClustersSel_FPix=0
    nClustersSel_FPixD1=0
    nClustersSel_FPixD2=0
    nClustersSel_FPixD3=0
    
    nClusters_FPix=0
    nClusters_FPixD1=0
    nClusters_FPixD2=0
    nClusters_FPixD3=0
    
    
    
    
    clus_vec=ROOT.TVector3()
    pix_vec=ROOT.TVector3()
    jet_vec=ROOT.TVector3()
    
    
    # loop over clusters
    for c in range(0, t.clus_detId.size()):
        clus_vec.SetXYZ(t.clus_glX[c]-t.vtx_x, t.clus_glY[c]-t.vtx_y,t.clus_glZ[c]-t.vtx_z)
        pix_vec.SetXYZ(t.clus_glX[c], t.clus_glY[c],t.clus_glZ[c])
        dR_min=9999.
        jetpt=-9.
        pix_Z = pix_vec.Z() 
        pix_Phi = pix_vec.Phi()

        for j in range(0, t.jet_px.size()):
            jet_vec.SetXYZ(t.jet_px[j],t.jet_py[j],t.jet_pz[j])
            dR=clus_vec.DeltaR(jet_vec)
            if dR<dR_min:
                dR_min=dR
            h_dR.Fill(dR)
            if dR<0.4: 
                nClustersSel+= 1
                jetpt=jet_vec.Pt()
                break
                
       
        
        
        if t.clus_subdet[c]==0:
            h_dR_min_BPix.Fill(dR_min)
            nClusters_BPix+=1
            if dR_min<0.4:
                nClustersSel_BPix+=1
            
            if t.clus_lyrdsk[c]==1:
                h_dR_min_BPixL1.Fill(dR_min)
                nClusters_BPixL1+=1
                h2_Cluster_Z_vs_phi_BPixL1.Fill(pix_Z,pix_Phi)
                h2_Cluster_Z_vs_ntracks_BPixL1.Fill(pix_Z,t.clus_ntracks[c])
                h1_Cluster_Z_vs_nPixels_BPixL1.Fill(pix_Z,t.clus_pixADC[c].size()) 
                for adc in t.clus_pixADC[c]:
                    h1_Pixel_ADC_BPixL1.Fill(adc) 
		if t.clus_ntracks[c]>1:
		    h2_Cluster_Z_vs_phi_BPixL1_merged.Fill(pix_Z,pix_Phi)                       
                if dR_min<0.4:                    
                    nClustersSel_BPixL1+=1
                    h2_Cluster_dR_vs_jetpt_BPixL1.Fill(jetpt,dR_min)
                    if t.clus_ntracks[c]>1:
                        h2_Cluster_dR_vs_jetpt_BPixL1_merged.Fill(jetpt,dR_min)                       
                    
                    
            
            elif t.clus_lyrdsk[c]==2:
                h_dR_min_BPixL2.Fill(dR_min)
                nClusters_BPixL2+=1
                h2_Cluster_Z_vs_phi_BPixL2.Fill(pix_Z,pix_Phi)
		if t.clus_ntracks[c]>1:
		    h2_Cluster_Z_vs_phi_BPixL2_merged.Fill(pix_Z,pix_Phi)                       
                if dR_min<0.4:
                    nClustersSel_BPixL2+=1
                    h2_Cluster_dR_vs_jetpt_BPixL2.Fill(jetpt,dR_min)
                    if t.clus_ntracks[c]>1:
                        h2_Cluster_dR_vs_jetpt_BPixL2_merged.Fill(jetpt,dR_min)
                
            elif t.clus_lyrdsk[c]==3:
                h_dR_min_BPixL3.Fill(dR_min)
                nClusters_BPixL3+=1
                h2_Cluster_Z_vs_phi_BPixL3.Fill(pix_Z,pix_Phi)
		if t.clus_ntracks[c]>1:
		    h2_Cluster_Z_vs_phi_BPixL3_merged.Fill(pix_Z,pix_Phi)                       
                if dR_min<0.4:
                    nClustersSel_BPixL3+=1
                    h2_Cluster_dR_vs_jetpt_BPixL3.Fill(jetpt,dR_min)
                    if t.clus_ntracks[c]>1:
                        h2_Cluster_dR_vs_jetpt_BPixL3_merged.Fill(jetpt,dR_min)
            
            elif t.clus_lyrdsk[c]==4:
                h_dR_min_BPixL4.Fill(dR_min)
                nClusters_BPixL4+=1
                h2_Cluster_Z_vs_phi_BPixL4.Fill(pix_Z,pix_Phi)
		if t.clus_ntracks[c]>1:
		    h2_Cluster_Z_vs_phi_BPixL4_merged.Fill(pix_Z,pix_Phi)                       
                if dR_min<0.4:
                    nClustersSel_BPixL4+=1
                    h2_Cluster_dR_vs_jetpt_BPixL4.Fill(jetpt,dR_min)
                    if t.clus_ntracks[c]>1:
                        h2_Cluster_dR_vs_jetpt_BPixL4_merged.Fill(jetpt,dR_min)
       
    
        else:
            h_dR_min_FPix.Fill(dR_min)
            nClusters_FPix+=1
            if dR_min<0.4:
                nClustersSel_FPix+=1
                
            if t.clus_lyrdsk[c]==1:   
                h_dR_min_FPixD1.Fill(dR_min)
                nClusters_FPixD1+=1
                h2_Cluster_Z_vs_phi_FPixD1.Fill(pix_Z,pix_Phi)
		if t.clus_ntracks[c]>1:
                    h2_Cluster_Z_vs_phi_FPixD1_merged.Fill(pix_Z,pix_Phi)
                if dR_min<0.4:
                    nClustersSel_FPixD1+=1
                    h2_Cluster_dR_vs_jetpt_FPixD1.Fill(jetpt,dR_min)
                    if t.clus_ntracks[c]>1:
                        h2_Cluster_dR_vs_jetpt_FPixD1_merged.Fill(jetpt,dR_min)
                  
            elif t.clus_lyrdsk[c]==2:
                h_dR_min_FPixD2.Fill(dR_min)
                nClusters_FPixD2+=1
                h2_Cluster_Z_vs_phi_FPixD2.Fill(pix_Z,pix_Phi)
		if t.clus_ntracks[c]>1:
                    h2_Cluster_Z_vs_phi_FPixD2_merged.Fill(pix_Z,pix_Phi)
                if dR_min<0.4:
                    nClustersSel_FPixD2+=1
                    h2_Cluster_dR_vs_jetpt_FPixD2.Fill(jetpt,dR_min)
                    if t.clus_ntracks[c]>1:
                        h2_Cluster_dR_vs_jetpt_FPixD2_merged.Fill(jetpt,dR_min)
                    
            elif t.clus_lyrdsk[c]==3:
                h_dR_min_FPixD3.Fill(dR_min)
                nClusters_FPixD3+=1
                h2_Cluster_Z_vs_phi_FPixD3.Fill(pix_Z,pix_Phi)
		if t.clus_ntracks[c]>1:
                    h2_Cluster_Z_vs_phi_FPixD3_merged.Fill(pix_Z,pix_Phi)
                if dR_min<0.4:
                    nClustersSel_FPixD3+=1
                    h2_Cluster_dR_vs_jetpt_FPixD3.Fill(jetpt,dR_min)
                    if t.clus_ntracks[c]>1:
                        h2_Cluster_dR_vs_jetpt_FPixD3_merged.Fill(jetpt,dR_min)
                
        
        
    h_nClustersSel.Fill(nClustersSel)
    
    h_nClustersSel_BPix.Fill(nClustersSel_BPix)
    h_nClustersSel_BPixL1.Fill(nClustersSel_BPixL1)
    h_nClustersSel_BPixL2.Fill(nClustersSel_BPixL2)
    h_nClustersSel_BPixL3.Fill(nClustersSel_BPixL3)
    h_nClustersSel_BPixL4.Fill(nClustersSel_BPixL4)
    
    h_nClusters_BPix.Fill(nClusters_BPix)
    h_nClusters_BPixL1.Fill(nClusters_BPixL1)
    h_nClusters_BPixL2.Fill(nClusters_BPixL2)
    h_nClusters_BPixL3.Fill(nClusters_BPixL3)
    h_nClusters_BPixL4.Fill(nClusters_BPixL4)
    
    h_nClustersSel_FPix.Fill(nClustersSel_FPix)
    h_nClustersSel_FPixD1.Fill(nClustersSel_FPixD1)
    h_nClustersSel_FPixD2.Fill(nClustersSel_FPixD2)
    h_nClustersSel_FPixD3.Fill(nClustersSel_FPixD3)
    
    h_nClusters_FPix.Fill(nClusters_FPix)
    h_nClusters_FPixD1.Fill(nClusters_FPixD1)
    h_nClusters_FPixD2.Fill(nClusters_FPixD2)
    h_nClusters_FPixD3.Fill(nClusters_FPixD3)
   
        
        #print ("Cluster %i size: %i" %(c,t.clus_size[c]) )

    # loop over jets
    #for j in range(0, t.jet_px.size()):
        #print ("jet %i px: %f" %(j, t.jet_px[j]) )

    # print vertex info
    #print ("vertex x: %f" %(t.vtx_x) )

h_nClusters.Write()
h_nJets.Write()
h_nClustersSel.Write()
h_dR.Write()

h_dR_min_BPix.Write()
h_dR_min_FPix.Write()

h_dR_min_BPixL1.Write()
h_dR_min_BPixL2.Write()
h_dR_min_BPixL3.Write()
h_dR_min_BPixL4.Write()

h_dR_min_FPixD1.Write()
h_dR_min_FPixD2.Write()
h_dR_min_FPixD3.Write()

h_nClustersSel_BPix.Write()
h_nClustersSel_BPixL1.Write()
h_nClustersSel_BPixL2.Write()
h_nClustersSel_BPixL3.Write()
h_nClustersSel_BPixL4.Write()

h_nClusters_BPix.Write()
h_nClusters_BPixL1.Write()
h_nClusters_BPixL2.Write()
h_nClusters_BPixL3.Write()
h_nClusters_BPixL4.Write()

h_nClustersSel_FPix.Write()
h_nClustersSel_FPixD1.Write()
h_nClustersSel_FPixD2.Write()
h_nClustersSel_FPixD3.Write()

h_nClusters_FPix.Write()
h_nClusters_FPixD1.Write()
h_nClusters_FPixD2.Write()
h_nClusters_FPixD3.Write()

h1_Cluster_Z_vs_nPixels_BPixL1.Write()
h1_Pixel_ADC_BPixL1.Write()
h2_Cluster_Z_vs_phi_BPixL1_ratio = h2_Cluster_Z_vs_phi_BPixL1_merged.Clone()
h2_Cluster_Z_vs_phi_BPixL2_ratio = h2_Cluster_Z_vs_phi_BPixL2_merged.Clone()
h2_Cluster_Z_vs_phi_BPixL3_ratio = h2_Cluster_Z_vs_phi_BPixL3_merged.Clone()
h2_Cluster_Z_vs_phi_BPixL4_ratio = h2_Cluster_Z_vs_phi_BPixL4_merged.Clone()
                                                                       
h2_Cluster_Z_vs_phi_FPixD1_ratio = h2_Cluster_Z_vs_phi_FPixD1_merged.Clone()
h2_Cluster_Z_vs_phi_FPixD2_ratio = h2_Cluster_Z_vs_phi_FPixD2_merged.Clone()
h2_Cluster_Z_vs_phi_FPixD3_ratio = h2_Cluster_Z_vs_phi_FPixD3_merged.Clone()

h2_Cluster_Z_vs_phi_BPixL1_ratio.Divide(h2_Cluster_Z_vs_phi_BPixL1)
h2_Cluster_Z_vs_phi_BPixL2_ratio.Divide(h2_Cluster_Z_vs_phi_BPixL2)
h2_Cluster_Z_vs_phi_BPixL3_ratio.Divide(h2_Cluster_Z_vs_phi_BPixL3)
h2_Cluster_Z_vs_phi_BPixL4_ratio.Divide(h2_Cluster_Z_vs_phi_BPixL4)
                                        
h2_Cluster_Z_vs_phi_FPixD1_ratio.Divide(h2_Cluster_Z_vs_phi_FPixD1)
h2_Cluster_Z_vs_phi_FPixD2_ratio.Divide(h2_Cluster_Z_vs_phi_FPixD2)
h2_Cluster_Z_vs_phi_FPixD3_ratio.Divide(h2_Cluster_Z_vs_phi_FPixD3)

h2_Cluster_Z_vs_phi_BPixL1.Write()
h2_Cluster_Z_vs_ntracks_BPixL1.Write()
h2_Cluster_Z_vs_phi_BPixL2.Write()
h2_Cluster_Z_vs_phi_BPixL3.Write()
h2_Cluster_Z_vs_phi_BPixL4.Write()

h2_Cluster_Z_vs_phi_FPixD1.Write()
h2_Cluster_Z_vs_phi_FPixD2.Write()
h2_Cluster_Z_vs_phi_FPixD3.Write()

h2_Cluster_Z_vs_phi_BPixL1_merged.Write()
h2_Cluster_Z_vs_phi_BPixL2_merged.Write()
h2_Cluster_Z_vs_phi_BPixL3_merged.Write()
h2_Cluster_Z_vs_phi_BPixL4_merged.Write()
                                 
h2_Cluster_Z_vs_phi_FPixD1_merged.Write()
h2_Cluster_Z_vs_phi_FPixD2_merged.Write()
h2_Cluster_Z_vs_phi_FPixD3_merged.Write()

h2_Cluster_Z_vs_phi_BPixL1_ratio.SetName("h2_Cluster_Z_vs_phi_BPixL1_ratio")
h2_Cluster_Z_vs_phi_BPixL2_ratio.SetName("h2_Cluster_Z_vs_phi_BPixL2_ratio")
h2_Cluster_Z_vs_phi_BPixL3_ratio.SetName("h2_Cluster_Z_vs_phi_BPixL3_ratio")
h2_Cluster_Z_vs_phi_BPixL4_ratio.SetName("h2_Cluster_Z_vs_phi_BPixL4_ratio")
                                                                           
h2_Cluster_Z_vs_phi_FPixD1_ratio.SetName("h2_Cluster_Z_vs_phi_FPixD1_ratio")
h2_Cluster_Z_vs_phi_FPixD2_ratio.SetName("h2_Cluster_Z_vs_phi_FPixD2_ratio")
h2_Cluster_Z_vs_phi_FPixD3_ratio.SetName("h2_Cluster_Z_vs_phi_FPixD3_ratio")
                                
h2_Cluster_Z_vs_phi_BPixL1_ratio.Write()
h2_Cluster_Z_vs_phi_BPixL2_ratio.Write()
h2_Cluster_Z_vs_phi_BPixL3_ratio.Write()
h2_Cluster_Z_vs_phi_BPixL4_ratio.Write()
                                
h2_Cluster_Z_vs_phi_FPixD1_ratio.Write()
h2_Cluster_Z_vs_phi_FPixD2_ratio.Write()
h2_Cluster_Z_vs_phi_FPixD3_ratio.Write()

h2_Cluster_dR_vs_jetpt_BPixL1.Write()
h2_Cluster_dR_vs_jetpt_BPixL2.Write()
h2_Cluster_dR_vs_jetpt_BPixL3.Write()
h2_Cluster_dR_vs_jetpt_BPixL4.Write()

h2_Cluster_dR_vs_jetpt_FPixD1.Write()
h2_Cluster_dR_vs_jetpt_FPixD2.Write()
h2_Cluster_dR_vs_jetpt_FPixD3.Write()

h2_Cluster_dR_vs_jetpt_BPixL1_merged.Write()
h2_Cluster_dR_vs_jetpt_BPixL2_merged.Write()
h2_Cluster_dR_vs_jetpt_BPixL3_merged.Write()
h2_Cluster_dR_vs_jetpt_BPixL4_merged.Write()

h2_Cluster_dR_vs_jetpt_FPixD1_merged.Write()
h2_Cluster_dR_vs_jetpt_FPixD2_merged.Write()
h2_Cluster_dR_vs_jetpt_FPixD3_merged.Write()


fOut.Close()


