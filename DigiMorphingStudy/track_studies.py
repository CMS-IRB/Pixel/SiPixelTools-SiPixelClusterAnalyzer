# define commandline arguments
from optparse import OptionParser
from collections import Counter

usage = "Usage: python %prog [options] \n\nExample: python %prog [--maxEvents=100 --reportEvery=1 --inputFile=cluster_tree.root]"

parser = OptionParser(usage=usage)

parser.add_option('--maxEvents', type='int', action='store',
                  default=-1,
                  dest='maxEvents',
                  help='Number of events to process. -1 means all events')

parser.add_option('--reportEvery', type='int', action='store',
                  default=1,
                  dest='reportEvery',
                  help='Report every N events (default value is 100)')

parser.add_option('--inputFile', type='string', action='store',
                  default='cluster_tree.root',
                  dest='inputFile',
                  help='Input file (default value is \'cluster_tree.root\')')

parser.add_option('--outputFile', type='string', action='store',
                  default='cluster_histograms.root',
                  dest='outputFile',
                  help='Output file (default value is \'cluster_histograms.root\')')

parser.add_option("--clusters", dest="clusters", action='store_true',
                  help="Print out clusters",
                  default=False)

parser.add_option("--pixels", dest="pixels", action='store_true',
                  help="Print out pixels",
                  default=False)

parser.add_option("--jets", dest="jets", action='store_true',
                  help="Print out jets",
                  default=False)

parser.add_option("--vtx", dest="vtx", action='store_true',
                  help="Print out the primary vertex",
                  default=False)

(options, args) = parser.parse_args()



import ROOT
import glob

# configure ROOT's behavior
ROOT.gROOT.SetBatch()
ROOT.gStyle.SetOptStat("nemruoi")
ROOT.gROOT.ForceStyle()


# open input file
t = ROOT.TChain()
if ".root" not in options.inputFile:
    for file in glob.glob(options.inputFile+"/*"):
        #print file
        if file.endswith(".root"):
            t.Add(file+"/siPixelClusterAnalyzer/t_clusters")
else:
    # get pointer to the ROOT tree
    t.Add(options.inputFile+"/siPixelClusterAnalyzer/t_clusters")

# create output file
fOut = ROOT.TFile(options.outputFile, "RECREATE")

h1_nClusters=ROOT.TH1F('h1_nCluster',';duplicate clusters per tracks;',50,0,50)
h1_nClus_diff=ROOT.TH1F('h1_nClus_diff',';Difference in # of Clusters (Merge-Morph);',500,-250,250)

# total number of events
nEntries = t.GetEntries()
#print nEntries
# loop over events
trk_dict = {} 
for e in range(0, nEntries):
    if options.maxEvents > 0 and (e+1) > options.maxEvents :
        break

    # get event
    t.GetEntry(e)

    nClusters_BPixL1=0
    # loop over clusters
    for c in range(0, t.track_Id.size()):

	clus_Id = t.track_clusId[c]
        if t.clus_subdet[clus_Id]==0 and t.clus_lyrdsk[clus_Id]==1 and abs(t.clus_glZ[clus_Id])>20.0:

	    trk_Id = t.track_Id[c]
	    bx_Id = t.track_bx[c]
	    evt_Id = t.track_evtId[c]
	    det_Id = t.clus_detId[clus_Id]
	    trk_str = str(e)+"_"+str(trk_Id)+"_"+str(bx_Id)+"_"+str(evt_Id)+"_"+str(det_Id)
	    #trk_str = str(e)+"_"+str(det_Id)
            if trk_str not in trk_dict:
               trk_dict[trk_str]= [clus_Id]
            else:
               trk_dict[trk_str].append(clus_Id)
            #if trk_str not in trk_dict:
            #   trk_dict[trk_str]= t.clus_pixADC[clus_Id].size()
            #else:
            #   trk_dict[trk_str]+=t.clus_pixADC[clus_Id].size()

#for trk in trk_dict:
#    for n_clus in trk_dict[trk]:
#        h1_nClusters.Fill(n_clus)

t_merge = ROOT.TChain()
#for file in glob.glob("/STORE/ferencek/Pixel/mc/PixelClusterTrees/MergedClusters/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/*"):
for file in glob.glob("/STORE/ferencek/Pixel/mc/PixelClusterTrees/MergedClustersWithPixels/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/cluster_tree_1.root"):
    print file
    if file.endswith(".root"):
        t_merge.Add(file+"/siPixelClusterAnalyzer/t_clusters")

trk_c = 0

merge_trk_dict ={}

for e in range(0, nEntries):
    if options.maxEvents > 0 and (e+1) > options.maxEvents :
        break

    # get event
    t_merge.GetEntry(e)

    nClusters_BPixL1=0
    # loop over tracks
    for c in range(0, t_merge.track_Id.size()):

        clus_Id = t_merge.track_clusId[c]
        if t_merge.clus_subdet[clus_Id]==0 and t_merge.clus_lyrdsk[clus_Id]==1 and abs(t_merge.clus_glZ[clus_Id])>20.0:

            trk_Id = t_merge.track_Id[c]
            bx_Id = t_merge.track_bx[c]
            evt_Id = t_merge.track_evtId[c]
            det_Id = t_merge.clus_detId[clus_Id]
            trk_str = str(e)+"_"+str(trk_Id)+"_"+str(bx_Id)+"_"+str(evt_Id)+"_"+str(det_Id)
            if trk_str not in merge_trk_dict:
               merge_trk_dict[trk_str]= [clus_Id]
            else:
               merge_trk_dict[trk_str].append(clus_Id)
            #trk_str = str(e)+"_"+str(det_Id)
            #if trk_str not in merge_trk_dict:
            #   merge_trk_dict[trk_str]= t.clus_pixADC[clus_Id].size()
            #else:
            #   merge_trk_dict[trk_str]+=t.clus_pixADC[clus_Id].size()

            trk_c+=1

merge_trk_set = set(merge_trk_dict)
morph_trk_set = set(trk_dict)

merge_only_trks = merge_trk_set-morph_trk_set

morph_only_trks = morph_trk_set-merge_trk_set

common_trks = morph_trk_set & merge_trk_set

print len(merge_only_trks)
print len(morph_only_trks)
print len(common_trks)

merge_clus = 0
morph_clus = 0

outfile = open("Clus_info","wt")
for trk in common_trks:
    merge_det_id =  merge_trk_dict[trk]
    morph_det_id =  trk_dict[trk]   
    if len(merge_det_id)-len(morph_det_id) >0:
        out_str = ''
        print 'Nominal :'+trk+":"+str(merge_det_id)
        print 'Morph :'+trk+":"+str(morph_det_id)+"\n"
        out_str+='Nominal :'+trk+":"+str(merge_det_id)+'\n'
        out_str+='Morph :'+trk+":"+str(morph_det_id)+"\n"
        out_str+='Diff :'+str(abs(len(merge_det_id)-len(morph_det_id)))+"\n\n"
        outfile.write(out_str)
    #merge_clus+= len(merge_det_id)
    #morph_clus+= len(morph_det_id)
    #h1_nClus_diff.Fill(len(merge_det_id)-len(morph_det_id))
    #h1_nClus_diff.Fill(merge_det_id-morph_det_id)
outfile.close()

print "Non-Morphed Cluster: "+str(merge_clus)
print "Morphed Cluster: "+str(morph_clus)

print "Difference Morphed-Non-Morphed: "+str(morph_clus-merge_clus)

fOut.cd()

h1_nClusters.Write()
h1_nClus_diff.Write()

fOut.Close()


