# Instructions

To do after ntuples are produced:

1) Run the `morphing_plots.py` script to produce output files with required filled histograms

Example Syntax:
```
python morphing_plots.py --maxEvents=-1 --reportEvery=100 --inputFile=/STORE/ferencek/Pixel/mc/PixelClusterTrees/MorphedClustersWithPixels/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/cluster_tree_1.root
```
This script can be run in batch mode, using the `submitAnalysisJobs.py` script which will produce one output file per input file that can be `hadd`-ed at the end.

Hadded histogram output file names that already exist from last iteration:

With morphing applied -> `cluster_histograms_morph.root`

Without morphing applied -> `cluster_histograms_nomorph.root`

2) Next step is to run `compare_clusters.py` which automatically takes the above mentioned histogram files as input and produces all the canvases in the form of .png files.

## Ntuple location

With morphing algortihm applied:
```
/STORE/ferencek/Pixel/mc/PixelClusterTrees/MorphedClustersWithPixels/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/
```
Without morphing algorithm applied:
```
/STORE/ferencek/Pixel/mc/PixelClusterTrees/MergedClustersWithPixels/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/
```

## Working area

Path to working area on Lorien:
```
/users/samishra/Work/Clusterizer/CMSSW_10_6_25/src/SiPixelTools/SiPixelClusterAnalyzer/JSP2021
```
