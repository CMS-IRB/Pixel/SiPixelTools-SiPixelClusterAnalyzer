import os
import sys
import re
import datetime
import glob


from optparse import OptionParser

usage = "Usage: python %prog [options] \n\nExample: python %prog -c condor_analysis_jobs -s step1.py -i /STORE/ferencek/Pixel/mc/PixelClusterTrees/MergedClusters/RelValZpTT_1500_13UP18/PU25ns_106X_upgrade2018_realistic_v6-v2/"

# input parameters
parser = OptionParser(usage=usage)

parser.add_option('-c', '--condor', action='store',
                  dest='condor',
                  help='Condor folder name to which a time stamp is appended at execution time (This parameter is mandatory)')

parser.add_option('-s', '--script', action='store',
                  dest='script',
                  help='Python script (This parameter is mandatory)')

parser.add_option('-i', '--input', action='store',
                  dest='input',
                  help='Input folder (This parameter is mandatory)')


(options, args) = parser.parse_args()

# make sure all necessary input parameters are provided
if not (options.condor and options.script and options.input):
    print ('Madatory parameters missing')
    print ('')
    parser.print_help()
    sys.exit(1)


def processFile(folder, i, input):
    print ('---------------------------------------')
    print ('Processing file %s' % input)
    print ('---------------------------------------')

    # create jdl file
    jdl_path = os.path.join(folder, ('Job_%i.jdl' % i))
    jdl_file = open(jdl_path,'w')
    jdl_content = jdl_template
    jdl_content = re.sub('DUMMY_CONDORDIR',folder,jdl_content)
    jdl_content = re.sub('DUMMY_ID',str(i),jdl_content)
    jdl_content = re.sub('DUMMY_INPUT',input,jdl_content)
    jdl_file.write(jdl_content)
    jdl_file.close()

    # submit Condor job
    os.system('condor_submit ' + jdl_path)


jdl_template = """universe = vanilla
Notification = never
Executable = DUMMY_CONDORDIR/analysisJob.sh
Output = DUMMY_CONDORDIR/Job_DUMMY_ID.stdout
Error = DUMMY_CONDORDIR/Job_DUMMY_ID.stderr
Log = DUMMY_CONDORDIR/Job_DUMMY_ID.log
Arguments = DUMMY_ID DUMMY_INPUT
Queue
"""


bash_template = """#!/bin/bash

ID=$1
INPUT=$2

START_TIME=`/bin/date`
echo "Started at $START_TIME"
echo ""

cd /users/nmiljanic/CMSSW_10_6_25

source /cvmfs/cms.cern.ch/cmsset_default.sh
eval `scram runtime -sh`

cd DUMMY_CONDORDIR
python DUMMY_SCRIPT --maxEvents=-1 --reportEvery=10 --inputFile=${INPUT} --outputFile=cluster_histograms_${ID}.root

exitcode=$?

echo ""
END_TIME=`/bin/date`
echo "Finished at $END_TIME"
exit $exitcode
"""

# make condor job folder
condor_folder = os.path.join(os.getcwd(), options.condor + '_' + datetime.datetime.now().strftime("%Y%m%d%H%M%S"))
os.system('mkdir -p ' + condor_folder)

# copy Python script
os.system('cp -p %s %s' % (options.script, condor_folder))

# create Bash script
bash_path = os.path.join(condor_folder,'analysisJob.sh')
bash_script = open(bash_path,'w')
bash_content = bash_template
bash_content = re.sub('DUMMY_CONDORDIR',condor_folder,bash_content)
bash_content = re.sub('DUMMY_SCRIPT',os.path.basename(options.script),bash_content)
bash_script.write(bash_content)
bash_script.close()
os.system('chmod +x ' + bash_path)


for i, f in enumerate(glob.glob(os.path.join(options.input,'*.root'))):
    processFile(condor_folder, i, f)

print ('---------------------------------------')
